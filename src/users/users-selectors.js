import { createSelector } from 'reselect';

// Users redux state
const usersReducerSelector = state => state.users;

// Users list
const usersSelector = createSelector(
    usersReducerSelector,
    usersReducer => usersReducer.get('payload')
);

// Users is fetching
const usersIsFetchingSelector = createSelector(
    usersReducerSelector,
    usersReducer => usersReducer.get('isFetching')
);

// Users error
const usersErrorSelector = createSelector(
    usersReducerSelector,
    usersReducer => usersReducer.get('error')
);

export {
    usersSelector,
    usersIsFetchingSelector,
    usersErrorSelector
}