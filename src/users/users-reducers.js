import {Map, fromJS, List} from 'immutable';

import {
    FETCH_USERS_SUCCESS, FETCH_USERS_REQUEST, FETCH_USERS_ERROR
} from './users-actions';

const users = (state = Map({
    payload: List([]),
    isFetching: false,
    error: false
}), action) => {
    let newState;

    switch (action.type) {
        // Requests
        case FETCH_USERS_REQUEST:
            newState = state.merge({
                isFetching: true,
                error: false
            });
            return newState;

        // Success
        case FETCH_USERS_SUCCESS:
            newState = state.merge({
                payload: fromJS(action.payload),
                isFetching: false,
                error: false,
                created: false,
                updated: false,
                deleted: false
            });
            return newState;

        // Errors
        case FETCH_USERS_ERROR:
                newState = state.merge({
                    isFetching: false,
                    error: action.error,
                    errorMessage: action.errorMessage,
                    created: false,
                    updated: false,
                    deleted: false
                });
                return newState;
            default:
                return state;
    }
};

export default users;