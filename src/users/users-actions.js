import axios from 'axios';

// Libs
import utilsLib from '../common/libs/utils-lib';

// Constants
export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';

export const readUsers = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchUsersRequest());

            const res = await readUsersRequest();

            dispatch(utilsLib.response(res, fetchUsersSuccess, fetchUsersError));
        } catch (err) {
            dispatch(utilsLib.response(err, fetchUsersError));
        }
    };
};

const fetchUsersRequest = () => {
    return {
        type: FETCH_USERS_REQUEST
    };
};

const fetchUsersSuccess = (payload) => {
    return {
        type: FETCH_USERS_SUCCESS,
        payload
    };
};

const fetchUsersError = (errorMessage) => {
    return {
        type: FETCH_USERS_ERROR,
        errorMessage
    };
};

export const readUsersRequest = async () => {
    return await axios.get('https://jsonplaceholder.typicode.com/users');
}