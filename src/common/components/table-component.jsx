import React from 'react';

// components
import { Table } from 'antd';

// css
import 'antd/dist/antd.css'; 

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        // specify the condition of filtering result
        // here is that finding the name started with `value`
        onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.name.length - b.name.length,
        sortDirections: ['descend'],
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        // specify the condition of filtering result
        // here is that finding the email started with `value`
        onFilter: (value, record) => record.email.indexOf(value) === 0,
        sorter: (a, b) => a.email.length - b.email.length,
        sortDirections: ['descend'],
      },
      {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
        // specify the condition of filtering result
        // here is that finding the name started with `value`
        onFilter: (value, record) => record.city.indexOf(value) === 0,
        sorter: (a, b) => a.city.length - b.city.length,
        sortDirections: ['descend'],
      }
];

const TableComponent = (props) => {
    const { data } = props;

    let tableData = [];
    data?.forEach(element => {
        tableData.push({
            name: element.name,
            email: element.email,
            city: element.address.city
        });
    });

    return (
        <div>
            <Table dataSource={tableData} columns={columns} />
        </div>
    )
};

export default TableComponent;