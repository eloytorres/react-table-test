import React from 'react';

// css
import '../../App.css';

const FirstRowData = (props) => {
    const { data } = props;

    let firstUser = {
        name: data[0].name,
        email: data[0].email,
        city: data[0].address.city
    };

    return (
        <div className='first-user-data'>
            <h2>First row data:</h2>
            <div className='row'>
                <div className='column'>
                    <div className='row'>
                        <span style={{fontWeigth: 'bold'}}>Name:</span>
                        <span>{firstUser.name}</span>
                    </div>
                </div>
                <div className='column'>
                    <div className='row'>
                        <span style={{fontWeigth: 'bold'}}>Email:</span>
                        <span>{firstUser.email}</span>
                    </div>
                </div><div className='column'>
                    <div className='row'>
                        <span style={{fontWeigth: 'bold'}}>City:</span>
                        <span>{firstUser.city}</span>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default FirstRowData;