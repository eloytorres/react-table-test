import { combineReducers } from 'redux';

import users from '../../users/users-reducers';

const rootReducer = combineReducers({
    users
});

export default rootReducer;