/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';

import './App.css';
import { readUsers } from './users/users-actions';

// Components
import TableComponent from './common/components/table-component';

// Actions
import { usersSelector, usersIsFetchingSelector, usersErrorSelector } from './users/users-selectors';
import FirstRowData from './common/components/first-row-data';

// Component selector
const usersPageSelector = createSelector(
  usersSelector,
  usersIsFetchingSelector,
  usersErrorSelector,
  (users, usersIsFetching, usersError) => {
    users = users.toJS();
    users = (Array.isArray(users) && users) || [];
    return {
      users: users,
      usersIsFetching,
      usersError
    }
  }
);

// Redux dispatch hook
const useReduxDispatch = () => {
  const dispatch = useDispatch();
  return {
    readUsers: () => dispatch(readUsers())
  }
};

const App = () => {
  // Redux
  const { users } = useSelector(state => usersPageSelector(state));
  const { readUsers } = useReduxDispatch();

  // Read user list when rendering App
  useEffect(() => {
    readUsers();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          React test Ant table.
        </p>
        <FirstRowData data={users} />
        <TableComponent data={users} />
      </header>
    </div>
  );
}

export default App;
